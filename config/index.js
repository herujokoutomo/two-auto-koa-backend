module.exports = {
    port:3000,
    database:{
        host: "localhost",
        port: 3306,
        dbname: "two_auto_service_dev",
        user:"root",
        password:""
    },
    redis: {
        host: "localhost",
        port: 6379,
        keyPrefix: 'twa_',
        password: ""
    }
}