const Sequelize = require('sequelize');
const config = require('../config');

let dbInstance = null;

const setupDatabase = function () {
    dbInstance = new Sequelize(config.database.dbname, config.database.user, config.database.password, {
        host: config.database.host,
        dialect: 'mysql',
        operatorsAliases: false,

        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    });
}

const getDbInstance = function () {
    return dbInstance;
}

module.exports = {
    setupDatabase,
    getDbInstance
}