const Sequelize = require('sequelize');
const database = require('../utils/database');
const bcrypt = require("bcrypt");
const db = database.getDbInstance();

const User = db.define('users', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    balance: {
        type: Sequelize.DOUBLE,
        defaultValue: 0
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE
});

User.sync().then(() => {
    // Table created
});

module.exports = User;