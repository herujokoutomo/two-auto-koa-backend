const redis = require('redis');
const config = require('../config');
let redisClient = null;

function setup() {
    let options = {
        host: config.redis.host,
        port: config.redis.port,
        password: config.redis.password,
    }
    redisClient = redis.createClient(options);

    redisClient.on("error", function (err) {
        console.log("Redis Error " + err);
    });
}

function getInstance() {
    return redisClient;
}

function getCache(key) {
    return new Promise(function(resolve, reject) {
        redisClient.get(config.redis.keyPrefix+key, function(err, cache) {
            if(err) {
                reject(err);
            } else {
                let payload = JSON.parse(cache);
                resolve(payload);
            }
        });
    });
}

function setCache(key, payload, minutesExpire = 10) {
    return new Promise(function(resolve, reject) {
        try {
            redisClient.set(config.redis.keyPrefix+key, JSON.stringify(payload), 'EX', minutesExpire * 60);
            resolve(true);    
        } catch (error) {
            reject(error);
        }
    });
}

module.exports = {
    setup,
    getInstance,
    getCache,
    setCache
}