const jwt = require('jsonwebtoken');
const User = require('../schemas/user');

const authenticateUser = async function (ctx, next) {
    let authToken = ctx.request.header['authorization'];
    console.log('MIDD', authToken);
    if (authToken) {

        try {
            let decoded = jwt.decode(authToken);
            console.log(decoded);
            if (decoded !== null) {
                ctx.currentUser = await User.findOne({ email: decoded.email});
                next();
            } else {
                ctx.status = 401;
                ctx.body = 'Invalid token';
            }
        } catch (error) {
            ctx.status = 401;
            ctx.body = 'Unauthorized';
        }
    } else {
        ctx.status = 401;
        ctx.body = 'Unauthorized';
    }
}

module.exports = {
    authenticateUser
}