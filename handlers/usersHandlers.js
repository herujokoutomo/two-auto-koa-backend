const User = require('../schemas/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const register = async function (ctx) {
    let userInput = ctx.request.body;
    console.log(userInput);
    try {
        let newUser = await User.create({
            email: userInput.email,
            password: bcrypt.hashSync(userInput.password, 12)
        });
        ctx.body = newUser;
    } catch (error) {
        console.log('ERR', error);
        ctx.status = 400;
        ctx.body = JSON.stringify(error);
    }
}

const listUsers = async function (ctx, cacheService) {
    try {
        let cache = await cacheService.getCache('users');
        if(cache) {
            ctx.body = {
                cache: true,
                data: cache
            };
        } else {
            let users = await User.findAll();
            await cacheService.setCache('users', users);
            ctx.body = users;
        }
    } catch (error) {
        ctx.status = 400;
        ctx.body = JSON.stringify(error);    
    }
    
}

const profile = async function (ctx) {
    console.log('profile', ctx);
    ctx.body = ctx.currentUser;    
}

const login = async function (ctx) {
    try {
        let userInput = ctx.request.body;
        let user = await User.findOne({ email: userInput.email });

        let validPassword = bcrypt.compareSync(userInput.password, user.password);

        if (validPassword) {
            let token = jwt.sign({ email: userInput.email }, 'S3CRet!!');
            ctx.body = {
                token,
                user
            };
        } else {
            ctx.status = 400;
            ctx.body = "invalid password";
        }
    } catch (error) {
        console.log('ERR', error);
        ctx.status = 400;
        ctx.body = JSON.stringify(error);
    }
}

module.exports = {
    register,
    listUsers,
    login,
    profile
}