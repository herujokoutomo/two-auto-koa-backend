const Koa = require('koa');
const json = require('koa-json');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const Sequelize = require('sequelize');
const database = require('./utils/database');
const cacheService = require('./services/cacheService');

let cacheInstance = null;

const router = new Router();
const app = new Koa();

app.use(bodyParser());
app.use(json());
app.use(router.routes());

function middle(ctx, next) {
  console.log('MID');
  next();
}

async function bootstrap() {
  // serup cache
  cacheService.setup();
  cacheInstance = cacheService.getInstance();
  // console.log('CACHE INSTANCE', cacheInstance);

  // setup database
  database.setupDatabase();
  const db = database.getDbInstance();
  try {
    await db.authenticate();
    console.log('db authencticated');

    // setup tables
    const UserTable = require('./schemas/user');

    setupRoutes();
  } catch (error) {
    console.log(error);
    console.log('failed db authenctication');
  }
}

function setupRoutes() {
  const middlewares = require('./middlewares');
  const usersHandlers = require('./handlers/usersHandlers');

  router.get('/', (ctx) => {
    ctx.body = "2 Auto services";
  });

  router.post('/register', (ctx) => {
    return usersHandlers.register(ctx);
  });

  router.post('/login', (ctx) => {
    return usersHandlers.login(ctx);
  });

  router.get('/users', (ctx) => {
    return usersHandlers.listUsers(ctx, cacheService);
  });

  router.get('/profile', middlewares.authenticateUser, (ctx) => {
    return usersHandlers.profile(ctx);
  });
}

const server = app.listen(3000);
console.log(`Run in port : ${server.address().port}`);
bootstrap();