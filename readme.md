# Getting Started

#### Installation
to run the app is simple, first need to `npm install` 

#### Configuration
setup your mysql database config in the `config/index.js`
```
module.exports = {
    port:3000,
    database:{
        host: "localhost",
        port: 3306,
        dbname: "two_auto_service_dev",
        user:"root",
        password:""
    },
    redis: {
        host: "localhost",
        port: 6379,
        keyPrefix: 'twa_',
        password: ""
    }
}
```

#### Run the app
just simply run `node index.js`